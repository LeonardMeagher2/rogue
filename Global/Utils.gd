extends Node


func capture_node_image(image_texture:ImageTexture, size:Vector2, node:Spatial):
	if size and node:
		var viewport = Viewport.new()
		viewport.size = size
		viewport.transparent_bg = true
		viewport.hdr = false
		viewport.usage = Viewport.USAGE_3D
		viewport.render_target_clear_mode = Viewport.CLEAR_MODE_ALWAYS
		viewport.render_target_update_mode = Viewport.UPDATE_ALWAYS
		viewport.render_target_v_flip = true
		
		var copy := node.duplicate(DUPLICATE_GROUPS)
		for child in copy.get_children():
			if not child.is_in_group("_thumbnail"):
				child.queue_free()
		copy.translation = Vector3()
		viewport.add_child(copy)
		add_child(viewport)
		yield(get_tree(),"idle_frame")
		var img = viewport.get_texture().get_data()
		image_texture.create_from_image(img)
		viewport.queue_free()
		

func merge(a, b, deep = false):
	var type = typeof(a)
	if type == typeof(b) and (type == TYPE_DICTIONARY or type == TYPE_ARRAY):
		if type == TYPE_DICTIONARY:
			for key in b:
				if deep:
					a[key] = merge(a.get(key), b[key])
				else:
					a[key] = b[key]
			return a
		elif type == TYPE_ARRAY:
			a.resize(max(a.size(), b.size()))
			for i in b.size():
				if deep:
					a[i] = merge(a[i], b[i])
				else:
					a[i] = b[i]
		else:
			return a + b
	else:
		return b
