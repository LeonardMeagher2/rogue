extends Node

signal before_turn(node_path)
signal after_turn(node_path)
signal turn_started(node_path)
signal turn_ended(node_path)

var queue : = []
var current_node = @"" setget set_current_node,get_current_node
var turn:int = 0
var seconds:float = 0.0

func set_current_node(v):pass
func get_current_node() -> Node:
	return get_node_or_null(current_node)

func add(node:Node):
	queue.append(node.get_path())
func has(node:Node):
	return node.get_path() in queue

func _process(_delta):
	set_process(false)
	turn += 1
	seconds += 12.0
	
	var node_path:NodePath = @"" if queue.empty() else queue.pop_front()
	
	emit_signal("before_turn", node_path)
	if node_path:
		var node:Node = get_node_or_null(node_path)
		if node:
			if node.has_method("take_turn"):
				current_node = node_path
				emit_signal("turn_started",node_path)
				node.call_deferred("take_turn",turn)
				yield(self,"turn_ended")
				current_node = @""
			else:
				printerr(node.get_path(), " doesn't have method \"take_turn\"!")
	emit_signal("after_turn", node_path)
	set_process(true)
	
func end_turn():
	if current_node:
		call_deferred("emit_signal","turn_ended",current_node)
	
func _ready():
	set_process(false)
	Saver.connect("new_world",self,"_reset")
	Saver.connect("load_world",self,"_reset")

func _reset(world_save:ConfigFile):
	queue = []
	current_node = @""
	turn = 0
	seconds = 0.0
	set_process(true)
	
	
