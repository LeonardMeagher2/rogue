extends Node

const _n = ["one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"];
const _s = ["zeroth", "first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth", "tenth", "eleventh", "twelfth", "thirteenth", "fourteenth", "fifteenth", "sixteenth", "seventeenth", "eighteenth", "nineteenth"];
const _p = ["twent", "thirt", "fourt", "fift", "sixt", "sevent", "eight", "ninet"];
const _c = ["hundred", "thousand", "million", "billion", "trillion", "quadrillion", "quintillion"];

func number_to_string(i:int):
	if i < 20:
		return _n[i - 1]
	if i < 100:
		return _p[i/10 - 2] + "y " + _n[i%10 - 1]
	return _n[i/100 - 1] + " " + _c[0] + " " + number_to_string(i/100 * 100)

func ordinal(i:int) -> String:
	var b:int = log(i) / log(10);
	
	if i < 20:
		return _s[i]
	if b==1: # between 20 and 99
		if i % 10 == 0:
			return _p[i/10 - 2] + "ieth"
		return _p[i/10 - 2] + "y " + _s[i % 10]
	if b==2: # between 100 and 999
		var e:int = i/100
		return _n[e - 1] + " " + _c[0] + " " + ordinal(i - (e * 100))
	
	# > 1000
	var m:int = b%3 + 1
	var cm:int = b/3
	var e:int = pow(10,b-m+1)
	var x:int = i/e
	
	return number_to_string(x) + " " + _c[cm] + " " + ordinal(i - (x * e))
	
func roll(dice:String) -> int:
	var parts : = dice.strip_edges().split(" ",false)
	var total:float = 0
	for part in parts:
		var numbers = part.split("d")
		for i in numbers[0].to_int():
			total += rand_range(1, numbers[1].to_int())
	
	return int(total)
	
func max_roll(dice:String) -> int:
	var parts : = dice.strip_edges().split(" ",false)
	var total:int = 0
	for part in parts:
		var numbers = part.split("d")
		total += numbers[0].to_int() * numbers[1].to_int()
	return total
	
