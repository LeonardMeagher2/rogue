extends Node

"""
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                       !
! Context should be used in _enter_tree !
!                                       !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"""

const META_KEY = "shared_context"

class Context extends Reference:
	var data = null
	var parent:Context
	var node_path:NodePath
	
	func _init(data = null, parent:Context = null):
		if data != null:
			self.data = data as Dictionary
		self.parent = parent

	func find_property(property):
		if data and data.has(property):
			return self
		
		var p = parent
		while(p):
			if p.data and p.data.has(property):
				return p
			p = p.parent
		
		return null
		
	func make_passthrough():
		data = null
		for sig in get_signal_list():
			if has_user_signal(sig.name):
				for con in get_signal_connection_list(sig.name):
					disconnect(con["signal"], con.target, con.method)
		
	func is_passthrough():
		return data == null
		
	func _set(property, value):
		# we have the property exit early
		if data and data.has(property):
			data[property] = value
			return true
		
		var p = parent
		var nearest_parent = null
		# search for parents who might have this property
		while(p):
			if not nearest_parent and p.data:
				# keep track of the closest parent that is not a passthrough
				nearest_parent = p
			if p.data and p.data.has(property):
				p.data[property] = value
				return true
			p = p.parent
			
		# parents did not have this property, set it on this context
		if not data:
			# we're an passthrough context so use the nearest parent's
			if nearest_parent:
				nearest_parent.data[property] = value
				return true
			data = {}
		# we either aren't a passthrough or there were no other options
		data[property] = value
		return true
		
	func assign(state:Dictionary):
		# assign state directly to this context
		if not data:
			data = {}
		for key in state:
			data[key] = state[key]
		return true
	
	func _get(property):
		if parent and (data == null or data.has(property) == false):
			return parent._get(property)
		if not data:
			return null
		return data.get(property)
		
	func get(property, default = null):
		if parent and (data == null or data.has(property) == false):
			return parent.get(property, default)
		if not data:
			return default
		return data.get(property, default)
	
	func on(event_name:String, target:Object, method:String, binds:Array = [], flags:int = 0):
		if has_user_signal(event_name) == false:
			var p = parent
			var nearest_parent = null
			while(p):
				if not nearest_parent and not p.is_passthrough():
					nearest_parent = p
				if p.has_user_signal(event_name):
					return p.connect(event_name, target, method, binds, flags)
				p = p.parent
			
			if is_passthrough() and nearest_parent:
				# we're a passthrough so attach to the nearest parent
				nearest_parent.add_user_signal(event_name)
				return nearest_parent.connect(event_name, target, method, binds, flags)
			# no signal was found on any parents
			add_user_signal(event_name)
		
		return connect(event_name, target, method, binds, flags)
	
	func off(event_name:String, target:Object, method:String):
		if is_connected(event_name, target, method):
			return disconnect(event_name, target, method)
		if parent:
			return parent.off(event_name, target, method)
	
	func fire(event_name:String, args:Array = []):
		if parent:
			parent.fire(event_name, args)
		if not has_user_signal(event_name):
			add_user_signal(event_name)
		callv("emit_signal", [event_name] + args)
	

var stack:Array = []

func _update_context(context:Context, node_ref:WeakRef):
	var node = node_ref.get_ref()
	if not node:
		return
	node.set_meta(META_KEY, context)
	context.node_path = node.get_path() if node.is_inside_tree() else @""
	
	var p = node.get_parent()
	while(p):
		if p.has_meta(META_KEY):
			var parent_context = p.get_meta(META_KEY)
			context.parent = parent_context
			return
		p = p.get_parent()
	
	context.parent = null
	return

func _remove_context(node_ref:WeakRef):
	var node = node_ref.get_ref()
	var context = node.get_meta(META_KEY)
	node.remove_meta(META_KEY)
	if context:
		context.node_path = @""

func _node_added(node:Node) -> void:
	if stack.size() == 0 or node == self:
		return
	
	var context = stack.pop_back()
	_update_context(context, weakref(node))
	if node.is_connected("tree_entered", self, "_update_context"):
		node.connect("tree_entered", self, "_update_context", [context, weakref(node)])
	node.connect("tree_exiting", self, "_remove_context", [weakref(node)], CONNECT_ONESHOT)

func _enter_tree():
	get_tree().connect("node_added", self, "_node_added")

func use(data = null) -> Context:
	var context = Context.new(data);
	stack.append(context)
	return context

func from(node:Node) -> Context:
	var p = node
	while(p):
		if p.has_meta(META_KEY):
			return p.get_meta(META_KEY)
		p = p.get_parent()
	return null
