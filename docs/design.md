# Game Identity
Multiplayer, squad based, turn bsaed, RPG; Control one or many characters in a persistant open world where you can give your characters jobs, fight against enemy factions and experience the world.

# Design Pillars

## Comfortability
I like to play games on my tv with my wife or brother, and it's even better if I can do that but online with other friends the game needs to work on a controller and be intuitive enough but allow complex actions

## Tactics
I like squad based games, or even solo rpgs where you can do a lot of complex actions, but I also like large scale tactics too, and I'm hoping to bring that to this rpg experience where eventually you could actually control a large number of characters or influence them to do what you want

## Dynamic experiences
I want mechanics that allow interesting scenarios to unfold some possibly scripted, but not.

# Genre - Story - Mechanics

# Features

# Interface

# Art Style

# Music and Sound

# Road Map
