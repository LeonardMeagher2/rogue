extends "res://Components/Behavior/Behavior.gd"

var current_job_path:NodePath

func _ready():
	add_to_group("actor_ai")

func take_turn(turn:int):
	# here we would actually take into account costs and job requirements, and if our current job changes, stop it
	while ctx.get("is_dead",false) == false and current_turn_ap > 0 and is_inside_tree():
		
		var job_stack = PriorityQueue.new()
		for child in get_children():
			if child is Job and child.enabled:
				var cost = child.compute_minimum_cost()
				if cost <= current_turn_ap:
					job_stack.insert(cost, get_path_to(child))
				else:
					child.clean()
		
		var new_job = false
		
		if not job_stack.empty():
			new_job = get_node_or_null(job_stack.pop_front())
			while job_stack.empty() == false:
				var job = get_node_or_null(job_stack.pop_front())
				if job:
					job.clean()
		
		
		if not new_job:
			if current_job_path:
				var current_job = get_node_or_null(current_job_path)
				current_job_path = @""
				current_job.clean()
			
			ctx.fire("bored")
			current_turn_ap = 0.0
			end_turn()
			return
		
		if current_job_path:
			var current_job = get_node_or_null(current_job_path)
			if current_job != new_job:
				current_job_path = get_path_to(new_job)
				current_job.clean()
		else:
			current_job_path = get_path_to(new_job)
		
		new_job.run()
		yield(new_job,"done")
	emit_signal("jobs_done")
	end_turn()
	
