extends Resource

export var level:int = 0
export(float,0.0,1.0) var vitality:float = 0.1
export var attack_range:int = 1
export var speed:float = 1.0
