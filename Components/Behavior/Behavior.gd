extends "res://Components/Component.gd"
class_name Behavior

signal jobs_done

const Dijkstra = preload("res://utils/Dijkstra.gd")

export var stats:Resource = load("res://Components/Behavior/BehaviorStats.gd").new()
var current_turn_ap:float
var turn_starting_position


func get_max_ap():
	return int(5 + stats.level*0.5)

var ctx:SharedContext.Context
	
func _enter_tree():
	ctx = SharedContext.use({
		"behavior": self
	})
func _ready():
	var actor_context = ctx.find_property("actor")
	if actor_context:
		actor_context.on("dead", self, "dead")
	ctx.on("get_path_data", self, "get_path_data")
	
	
	TurnManager.connect("before_turn",self,"before_turn", [], CONNECT_REFERENCE_COUNTED)
	TurnManager.connect("after_turn",self,"after_turn", [], CONNECT_REFERENCE_COUNTED)
	
func _exit_tree():
	TurnManager.disconnect("before_turn",self,"before_turn")
	TurnManager.disconnect("after_turn",self,"after_turn")
	
func before_turn(node_path:NodePath):
	if not enabled:
		return
	var recovery_points = get_max_ap() * stats.vitality
	current_turn_ap = clamp(current_turn_ap + recovery_points,0.0,get_max_ap())

func after_turn(node_path:NodePath):
	if current_turn_ap == get_max_ap() and not TurnManager.has(self):
		TurnManager.add(self)

func end_turn():
	TurnManager.end_turn()

func get_path_data(context:Dijkstra.Context):
	var costs = ctx.map.get_faction_relationships_to_node(ctx.actor)
	for position in costs:
		var cost = costs[position]
		if cost < Factions.HOSTILE_THRESHOLD:
			context.add_cost(position, "enemies", 7)
		elif cost > Factions.ALLY_THRESHOLD:
			context.add_cost(position, "allies", 1)
		else:
			context.add_cost(position, "actors", 1)
	
	context.add_weights({
		"allies":1,
		"actors":1,
		"enemies":1
	})

func dead():
	print(get_path(), " Died")
	enabled = false

#func _event(event_name:String, data, event_source:NodePath):
#	match event_name:
#		"save":
#			var actor_file = data as ConfigFile
#			actor_file.set_value("behavior", "ap", current_turn_ap)
#			var job_state = {}
#			var Job = load("res://Components/AI/Job.gd")
#			for child in get_children():
#				if child is Job:
#					job_state[child.name] = {
#						"filename": child.filename,
#						"script": child.get_script().resource_path,
#						"state": child.save()
#					}
#			actor_file.set_value("behavior", "job_state", job_state)
#
#		"load":
#			var actor_file = data as ConfigFile
#			current_turn_ap = actor_file.get_value("behavior", "ap", current_turn_ap)
#			var job_state = actor_file.get_value("behavior", "job_state")
#			if job_state:
#				var Job = load("res://Components/AI/Job.gd")
#				for child in get_children():
#					if child is Job:
#						remove_child(child)
#						child.queue_free()
#				for job_name in job_state:
#					var job_data = job_state[job_name]
#					var job
#					if job_data.filename:
#						job = load(job_data.filename).instance()
#					elif job_data.script:
#						job = load(job_data.script).new()
#
#					if job:
#						job.load(job_data.state)
#						add_child(job)
#						job.name = job_name

func get_movement_to_nearest(targets:Array) -> Dijkstra.DPath:
	if not targets or not targets.size():
		return Dijkstra.DPath.new()
	
	var pathing_context = ctx.map.pathing_context.duplicate(true)
	var map_position = ctx.map.world_to_map(ctx.actor.translation)
	ctx.fire("get_path_data",[pathing_context])
	var movement = Dijkstra.astar(pathing_context, map_position,targets)
	
	return movement
	
func get_walkable_dijkstra(from:Vector3 = get_meta("map_position"), max_ap:float = current_turn_ap) -> Dijkstra.DResult:
	var pathing_context = ctx.map.pathing_context.duplicate(true)
	ctx.fire("get_path_data",[pathing_context])
	var speed = max(ctx.get("speed",1.0) * ctx.get("walk_multiplier",1.0),0.0)
	return Dijkstra.build_dijkstra(pathing_context,[from],max_ap*speed+1,true)

func line_of_sight(to:Vector3, excludes:Array = []):
	var space = ctx.actor.get_world().direct_space_state
	var hs = ctx.map.cell_size / 2.0
	var world_to = ctx.map.map_to_world(to) + hs
	var res = space.intersect_ray(ctx.actor.translation+hs, world_to, [ctx.actor] + excludes)
	return res
