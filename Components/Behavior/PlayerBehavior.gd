extends "res://Components/Behavior/Behavior.gd"

func _ready():
	add_to_group("actor_player")
	Saver.connect("save_world",self,"_save", [], CONNECT_REFERENCE_COUNTED)

func take_turn(turn:int):
	set_process(true)
	
func end_turn():
	set_process(false)
	.end_turn()


func _process(delta):
	if not enabled:
		return
	
	if current_turn_ap > 0.0:
		
		if get_child_count() > 0:
			var job = get_child(0) as Job
			if job:
				if current_turn_ap >= job.compute_minimum_cost():
					set_process(false)
					job.run()
					yield(job,"done")
					set_process(true)
			
				remove_child(job)
				if job.free_on_exit:
					job.queue_free()
	else:
		end_turn()

func _save(world_file:ConfigFile):
	world_file.set_value("players",ctx.actor.name, {
		"map":ctx.map.name
	})
