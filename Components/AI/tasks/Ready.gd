extends "res://Components/AI/Task.gd"

var target:NodePath = @""
var job = load("res://Components/AI/Job.gd").new()

func _ready():
	name = "Ready"
	job.enabled = false

func is_valid() -> bool:
	return not is_inf(job.compute_minimum_cost())

func run():
	var behavior = ctx.behavior
	behavior.add_child(job)
	behavior.move_child(job,0)
	pass

func _turn_started(node_path:NodePath):
	var t = get_node_or_null(target)
	if t and t.is_a_parent_of(get_node_or_null(node_path)) and is_valid():
		job.run()
	pass
	
func _enter_tree():
	TurnManager.connect("turn_started",self,"_turn_started", [], CONNECT_REFERENCE_COUNTED)
func _exit_tree():
	TurnManager.disconnect("turn_started",self,"_turn_started")
