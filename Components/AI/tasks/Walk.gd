extends "res://Components/AI/Task.gd"

const Dijkstra = preload("res://utils/Dijkstra.gd")
var movement:Dijkstra.DPath

var tween = Tween.new()

func _init():
	name = "Walk"
	add_child(tween)

func get_speed():
	var behavior = ctx.behavior
	return ctx.get("speed",1.0) * ctx.get("walk_multiplier",1.0)

func get_cost():
	return movement.total_cost / get_speed()

func get_minimum_cost() -> float:
	if movement.costs.size() == 0:
		return INF
	return movement.costs.front() / get_speed()

func is_valid() -> bool:	
	if get_speed() and movement and movement.path.size() and movement.costs.size():
		return true
	return false
	
func run():
	var behavior = ctx.behavior
	var cost_to_move = get_minimum_cost()
	var next_position = movement.path.pop_front()
	
	movement.costs.pop_front()
	
	behavior.current_turn_ap -= cost_to_move
	movement["total_cost"] -= cost_to_move
	# replace this with a move event?
	var final_position = ctx.map.map_to_world(next_position.x, next_position.y, next_position.z)
	tween.interpolate_property(ctx.actor, "translation", ctx.actor.translation, final_position, 0.1, Tween.TRANS_LINEAR,Tween.EASE_IN_OUT)
	tween.start()
	yield(tween,"tween_all_completed")
	done()
