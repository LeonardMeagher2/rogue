extends "res://Components/AI/Task.gd"

var target:NodePath = @""

var tween = Tween.new()

func _init():
	name = "AttackActor"
	add_child(tween)

func get_cost():
	return 1.0

func run():
	var behavior = ctx.behavior
	var actor:Actor = behavior.get_node_or_null(target) as Actor
	var pos:Vector3 = ctx.actor.translation
	
	if not behavior or not actor:
		target = @""
		done()
		return
	
	behavior.current_turn_ap -= get_cost()
	target = @""
	var data := {
		"damage": Numbers.roll("1d20"),
		"actor_path": actor.get_path(),
		"map_position": ctx.map.world_to_map(actor.translation)
	}
	ctx.fire("attack",[data,ctx.actor.get_path()])
	
	if data.damage:
		var target_context = SharedContext.from(actor)
		target_context.fire("take_damage", [data, ctx.actor.get_path()])
		tween.interpolate_property(ctx.actor,"translation",pos, pos.linear_interpolate(actor.translation,0.5),0.3,Tween.TRANS_BACK,Tween.EASE_OUT)
		tween.start()
	
		yield(tween,"tween_all_completed")
		ctx.actor.translation = pos
	done()
	
func is_valid() -> bool:
	var actor = get_node_or_null(target) as Actor
	
	if actor:
		if ctx.get("is_dead",false):
			return false
		var map_position = ctx.map.world_to_map(ctx.actor.translation)
		var other_map_position = ctx.map.world_to_map(actor.translation)
		# currently only supports adjacent attacks
		if map_position.distance_to(other_map_position) > 1.0:
			return false
	else:
		return false
	
	return true
