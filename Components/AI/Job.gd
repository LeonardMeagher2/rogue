extends Node
class_name Job

signal done

export var enabled:bool = true
export var free_on_exit:bool = false
var is_running := false
var is_finished := false
# These are for jobs that want to alter the assumed cost without impacting action points
var base_total_cost := 0.0
var base_minimum_cost := 0.0

var ctx:SharedContext.Context

func _enter_tree():
	ctx = SharedContext.use()

func run():
	is_running = true
	is_finished = false
	var job
	var task_count = get_child_count()
	var tasks_finished = 0
	while get_child_count():
		var task = get_child(0)
		tasks_finished += 1
		while task.is_valid():
			if not task.get_minimum_cost() <= ctx.behavior.current_turn_ap:
				tasks_finished -= 1
				break
			task.run()
			yield(task,"done")
		
		remove_child(task)
		if task.free_on_exit:
			task.queue_free()
	if task_count == tasks_finished:
		is_finished = true
	done()

func clean():
	for child in get_children():
		if child is Task:
			remove_child(child)
			if child.free_on_exit:
				child.queue_free()
	
func setup() -> bool:
	return true

func done():
	is_running = false
	call_deferred("emit_signal","done")

func compute_minimum_cost() -> float:	
	if not get_child_count():
		var res = setup()
		while typeof(res) == TYPE_OBJECT and res is GDScriptFunctionState:
			res = yield(res, "completed")
		if res == false:
			return INF
	var minimum := INF
	for child in get_children():
		if child.is_valid():
			minimum = min(child.get_minimum_cost(), minimum)
	return minimum + base_minimum_cost

func compute_total_cost() -> float:
	if not get_child_count():
		var res = setup()
		while typeof(res) == TYPE_OBJECT and res is GDScriptFunctionState:
			res = yield(res, "completed")
		if res == false:
			return INF
	var total : = 0.0
	for child in get_children():
		if child.is_valid():
			total += child.get_cost()
		else:
			total = INF
			break
	return total + base_total_cost
	
func save():
	return {
		"enabled": enabled,
		"base_total_cost": base_total_cost,
		"base_minimum_cost": base_minimum_cost
	}
func load(state):
	enabled = state.get("enabled", enabled)
	base_total_cost = state.get("base_total_cost",base_total_cost)
	base_minimum_cost = state.get("base_minimum_cost", base_minimum_cost)
