extends Node
class_name Task

signal done

export var free_on_exit:bool = false

var ctx:SharedContext.Context

func _enter_tree():
	ctx = SharedContext.use()

func run():
	pass
	
func is_valid() -> bool:
	return false
	
func done():
	call_deferred("emit_signal","done")


# Setters and getters
func get_cost() -> float: return INF
func get_minimum_cost() -> float: return get_cost()
