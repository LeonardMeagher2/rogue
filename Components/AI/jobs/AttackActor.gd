extends "res://Components/AI/Job.gd"

var target:NodePath

var walk_to = load("res://Components/AI/tasks/Walk.gd").new()
var attack = load("res://Components/AI/tasks/AttackActor.gd").new()

func _init():
	name = "AttackActor"

func setup() -> bool:
	var behavior = ctx.behavior
	if ctx.get("is_passive",false):
		return false
	if Factions.is_node_passive(ctx.actor):
		return false
	
	var node = get_node_or_null(target) as Actor
	if not node:
		return false
	
	if Factions.is_node_ally_to_node(ctx.actor, node):
		base_minimum_cost = 10
	else:
		base_minimum_cost = 0
	
	var map_position = ctx.map.world_to_map(node.translation)
#	var los = behavior.line_of_sight(map_position)
#	if los and los.collider != node:
#		return false
	
	var movement = behavior.get_movement_to_nearest([map_position])
	
	if not movement.path.size():
		return false
	
	add_child(walk_to)
	add_child(attack)
	
	movement.path.pop_back()
	movement["total_cost"] -= movement.costs.pop_back()
	
	walk_to.movement = movement
	
	# get actor using position, and remove cost since we won't be moving onto that tile
	attack.target = target
	
	return true

func save():
	var state = .save()
	return Utils.merge(state,{
		"target": target
	})

func load(state):
	.load(state)
	target = state.get("target",target)
