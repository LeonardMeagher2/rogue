extends "res://Components/AI/Job.gd"

var walk_to = load("res://Components/AI/tasks/Walk.gd").new()
var attack = load("res://Components/AI/tasks/AttackActor.gd").new()

func _init():
	name = "AttackHostiles"

func setup() -> bool:
	
	var behavior = ctx.behavior
	if ctx.get("is_passive",false):
		return false
	if Factions.is_node_passive(ctx.actor):
		return false
	
	var targets = []
	var by_position = {}
	
	for child in ctx.map.get_children():
		if child == ctx.actor:
			continue
		
		if child is Actor and Factions.is_node_hostile_to_node(ctx.actor, child):
			var child_context = SharedContext.from(child)
			if not child_context:
				continue
			
			if child_context.get("is_dead", false) == false:
				
				var map_position = child_context.map.world_to_map(child.translation)
#				var los = behavior.line_of_sight(map_position)
#				if los and los.collider != child:
#					continue
				targets.append(map_position)
				by_position[map_position] = child.get_path()
	targets.sort()
	
	if targets.size() == 0:
		return false
	
	var movement_to_nearest = behavior.get_movement_to_nearest(targets)
	if movement_to_nearest.path.size() == 0:
		return false
	
	add_child(walk_to)
	add_child(attack)
	
	walk_to.movement = movement_to_nearest
	
	# get actor using position, and remove cost since we won't be moving onto that tile
	attack.target = by_position[walk_to.movement.path.pop_back()]
	walk_to.movement["total_cost"] -= walk_to.movement.costs.pop_back()
	
	return true
