extends "res://Components/AI/Job.gd"

var target:NodePath
var walk_task = load("res://Components/AI/tasks/Walk.gd").new()

func _init():
	name = "Follow"

func setup():
	var behavior = ctx.behavior
	var actor = get_node_or_null(target)
	if not actor:
		return false
	
	var map_position = ctx.map.world_to_map(ctx.actor.translation)
	
	add_child(walk_task)
	walk_task.movement = behavior.get_movement_to_nearest([map_position])
	
	return true

func save():
	var state = .save()
	return Utils.merge(state,{
		"target": target
	})
func load(state):
	.load(state)
	target = state.get("target",target)
