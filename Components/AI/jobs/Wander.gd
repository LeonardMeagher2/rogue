extends "res://Components/AI/Job.gd"

const Dijkstra = preload("res://utils/Dijkstra.gd")

var Walk = load("res://Components/AI/tasks/Walk.gd")

func _init():
	name = "Wander"

func setup():
	var behavior = ctx.behavior
	var ap = behavior.current_turn_ap
	
	var last_position = ctx.map.world_to_map(ctx.actor.translation)
	while ap > 0:
		var dijkstra = behavior.get_walkable_dijkstra(last_position,ap)
		var map_positions = dijkstra.came_from.keys()
		
		if map_positions.size() > 1:
			var i:int = rand_range(0,map_positions.size())
			last_position = map_positions[i]
			
			var walk_task = Walk.new()
			walk_task.movement = Dijkstra.build_path_from_dijkstra(dijkstra,last_position, true)
			add_child(walk_task)
			walk_task.free_on_exit = true
			if ap < walk_task.get_minimum_cost():
				break;
			ap -= walk_task.get_cost()
		else:
			break
	
	return true
