extends "res://Components/Component.gd"

const Dijkstra = preload("res://utils/Dijkstra.gd")

var Walk = load("res://Components/AI/tasks/Walk.gd")
var AttackActor = load("res://Components/AI/jobs/AttackActor.gd")
var ctx:SharedContext.Context
var hp = 100

func _enter_tree():
	ctx = SharedContext.use()
	
func _ready():
	ctx.on("get_possible_jobs", self, "get_possible_jobs")
	ctx.on("get_path_data", self, "get_path_data")
	ctx.on("take_damage", self, "take_damage")
	ctx.walk_multiplier = 1.0
	ctx.is_dead = false

class BodyPartIterator extends Reference:
	var start:Node
	var current:Node
	var current_child := 0
	
	var _recursive_iterator = null
	
	func _init(start:Node):
		self.start = start
		current = null
	
	func should_continue():
		return current_child < start.get_child_count()
	
	func _iter_init(arg):
		current = null
		var i = 0
		while( i < start.get_child_count()):
			if start.get_child(i) is BodyPart:
				current = start.get_child(i)
				current_child = i
				break
		return should_continue()
	
	func _iter_next(arg):
		if _recursive_iterator:
			if _recursive_iterator.should_continue():
				return _recursive_iterator._iter_next(arg)
			else:
				_recursive_iterator = null
		else:
			if current.get_child_count():
				_recursive_iterator = get_script().new(current)
				_recursive_iterator._iter_init(arg)
		
		if not _recursive_iterator:
			current = null
			current_child += 1
			
			var i = current_child
			while( i < start.get_child_count()):
				if start.get_child(i) is BodyPart:
					current = start.get_child(i)
					current_child = i
					break
			
		return should_continue()
			
	func _iter_get(arg):
		if _recursive_iterator:
			return _recursive_iterator._iter_get(arg)
		return current

func find_attacking_parts() -> Array:
	var res = []
	for body_part in BodyPartIterator.new(self):
		if body_part.is_used_for_attacking():
			res.append(get_path_to(body_part))
	return res
	
func get_body_parts(start:Node = self) -> Array:
	var res = []
	if start is BodyPart:
		res.append(start)
	for child in start.get_children():
		if child is BodyPart:
			res += get_body_parts(child)
	return res

func apply_state():
	if ctx.is_dead:
		return
	
	ctx.walk_multiplier = 1.0
	ctx.is_dead = false
	
	if get_child_count() > 0:
		
		ctx.walk_multiplier = 0.25
		ctx.is_dead = true
		
		for body_part in BodyPartIterator.new(self):
			if body_part.is_dead == false and body_part.config.is_for_standing:
				ctx.walk_multiplier = 1.0
			if body_part.is_dead == false and body_part.config.is_vital:
				ctx.is_dead = false

func make_walk_job(behavior:Behavior, map_position:Vector3):
	var job = Job.new()
	job.free_on_exit = true
	job.name = "Walk"
	var walk_task = Walk.new()
	walk_task.free_on_exit = true
	walk_task.movement = behavior.get_movement_to_nearest([map_position])
	job.add_child(walk_task)
	return job

func get_possible_jobs(data, event_source:NodePath = ctx.actor.get_path()):
	
	var behavior = data.get("behavior") as Behavior
	var actor_path = data.get("actor_path") as NodePath
	var map_position = data.get("map_position") as Vector3
	var jobs = data.get("jobs") as Array
	
	if not behavior or jobs == null:
		return

	if event_source == ctx.actor.get_path():
		if actor_path:
			var attack_job = AttackActor.new()
			attack_job.free_on_exit = true
			attack_job.name = "Attack"
			attack_job.target = actor_path
			jobs.append(attack_job)
		else:
			var actor_map_position = ctx.map.world_to_map(ctx.actor.translation)
			if map_position != actor_map_position:
				jobs.append(make_walk_job(behavior, map_position))

func get_path_data(pathing_context:Dijkstra.Context):
	pathing_context.neighbor_mask = Dijkstra.NEIGHBORS.GROUND
	pathing_context.add_weights({
		"wall":INF
	})

func take_damage(data, event_source):
	hp -= data.damage
	hp = clamp(hp, 0.0, 100.0)
	if hp == 0:
		ctx.is_dead = true
		ctx.fire("dead")
