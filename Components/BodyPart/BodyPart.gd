extends Node
class_name BodyPart

export var config:Resource
export var individual_name:String = ""
export var individual_pluralized_name:String = ""
export(String, "Config", "Inherited", "Middle", "Left", "Right") var side = "Config" # You can use this to override which side the body part is on
export var is_dead := false

export(float, -100.0, 100.0) var starting_lifeforce = 100.0
onready var longterm_lifeforce = starting_lifeforce
onready var shortterm_lifeforce = starting_lifeforce

func is_used_for_attacking() -> bool:
	var parent:BodyPart = get_parent() as BodyPart
	var c:BodyPartConfig = config as BodyPartConfig
	
	var could_attack = (c.is_for_grabbing or c.is_for_standing or c.is_digit)
	
	if parent:
		var pc:BodyPartConfig = parent.config as BodyPartConfig
		return pc.is_limb and could_attack
		
	return could_attack

func get_side() -> String:
	if side == "Config":
		if config and config is BodyPartConfig:
			var c = config as BodyPartConfig
			if c.side == "Inherited":
				var p = get_parent() as BodyPart
				return p.get_side() if p else ""
			return c.side
		return ""
	elif side == "Inherited":
		var p = get_parent() as BodyPart
		return p.get_side() if p else ""
	return side

func get_full_name(plural:bool = false):
	var c = config as BodyPartConfig
	var full_name = PoolStringArray()
	
	var calculated_side = get_side()
	if calculated_side and not calculated_side == "Middle":
		full_name.append(calculated_side)
	
	if individual_name:
			full_name.append(individual_pluralized_name if plural else individual_name)
	elif plural:
			full_name.append(c.pluralized_name)
	else:
		if get_parent().is_class(get_class()):
			full_name.append(Numbers.ordinal(get_index()))
		full_name.append(c.name)
		
	return full_name.join(" ")

func _ready():
	TurnManager.connect("before_turn", self, "_before_turn")
	
func _before_turn(node_path:NodePath):
	shortterm_lifeforce += (longterm_lifeforce - shortterm_lifeforce) * config.repair_rate
	shortterm_lifeforce = clamp(shortterm_lifeforce, -100.0, longterm_lifeforce)
	longterm_lifeforce += (shortterm_lifeforce - longterm_lifeforce) *config.vulnerability
	longterm_lifeforce = clamp(longterm_lifeforce, -100.0, 100.0)
	
