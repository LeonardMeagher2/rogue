extends Resource
class_name BodyPartConfig

export var name:String = ""
export var pluralized_name:String = ""
export(String, "Inherited", "Middle", "Left", "Right") var side = "Inherited"

# Using similar properties to DF body parts

export var is_aperature:bool # an opening in the body if embedded can't be gouged
export var is_digit:bool # can gouge, body parts with these as direct children can also gouge
export var is_embedded:bool # embedded on the surface of the body, can't be cut off or grabbed, can't wrestle
export var is_internal:bool # can't be cut off, can't wrestle, can't be targeted, can be damaged if parent is damaged
export var is_joint:bool # if inside a limb, disables the limb, if outside a limb, the limb falls off
export var is_limb:bool # can initiate wrestling
export var is_socket:bool # body part breaks off and goes flying when broken
export var is_skeleton:bool
export var is_under_pressure:bool # will popout when parent is cut through
export var is_vital:bool # if all body parts with the same name are disabled, the creature dies

export var is_for_biting:bool
export var is_for_breathing:bool
export var is_for_circulation:bool # damage can cause slow disability of parent body part
export var is_for_flying:bool # if enough flying parts are damaged the creature can no longer fly
export var is_for_grabbing:bool
export var is_for_hearing:bool
export var is_for_nervous_function:bool # Damage disables everything in parent
export var is_for_seeing:bool
export var is_for_smelling:bool
export var is_for_standing:bool # if all body parts with this are disabled the character will fall
export var is_for_thought:bool # damage to this can disable behavior
export var is_for_airflow:bool # This can be stangled

export var damage_causes_nausea:bool
export var prevents_collapse:bool # this must be destroyed for the the parent to be destroyed

export var repair_rate:float = 0.05 # rate at which shortterm lifeforce moves towards longterm lifeforce
export var vulnerability:float = 0.01 # rate at which longterm lifeforce moves towards shortterm lifeforce






