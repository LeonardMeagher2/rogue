extends Node

enum NEIGHBORS {
	NONE = 0
	NORTH = 1
	SOUTH = 2
	EAST = 4
	WEST = 8
	UP = 16
	DOWN = 32
	
	ALL = 63 # NSEWUD
	GROUND = 47 # NSEWD
	FLAT = 15 # NSEW
}

class Context extends Reference:
	var costs = {}
	var edges = {}
	var weights = {}
	
	var neighbor_mask:int = NEIGHBORS.ALL
	
	var bounds:AABB
	
	func _init(bounds:AABB):
		self.bounds = bounds

	func has_edge(from:Vector3, to:Vector3):
		return edges.has(from) and edges[from].has(to)

	func remove_edge(from:Vector3, to:Vector3, bi_directional:bool = false) -> void:
		var key = [from,to]
		if edges.has(from):
			edges[from].erase(to)
			if edges[from].size() == 0:
				edges.erase(from)
		
		if costs.has(key):
			costs.erase(key)
			
		if bi_directional:
			remove_edge(to,from,false)

	
	func add_cost(position:Vector3, type:String, amount:float = 1.0) -> void:
		if not costs.has(type):
			costs[type] = []
		if not costs.has(position):
			costs[position] = {}
			
		var tile = costs.get(position)
		tile[type] = tile.get(type, 0.0) + amount
		
		if costs[type].has(position):
			if tile[type] == 0:
				costs[type].erase(position)
		else:
			costs[type].append(position)
		
		weights[type] = weights.get(type, 1.0)


	func add_edge_cost(from:Vector3, to:Vector3, type:String, amount:float = 1.0, bi_directional = false) -> void:
		var key = [from,to]
		
		if not edges.has(from):
			edges[from] = []
		edges[from].append(to)
		
		if not costs.has(type):
			costs[type] = []
		if not costs.has(key):
			costs[key] = {}
		var tile = costs.get(key)
		tile[type] = tile.get(type,0.0) + amount
		
		if costs[type].has(to):
			if tile[type] == 0.0:
				costs[type].erase(to)
		else:
			costs[type].append(to)
			
		if bi_directional:
			add_edge_cost(to,from,type,amount,false)
		else:
			weights[type] = weights.get(type, 1.0)
		


	func get_edge_cost(from:Vector3, to:Vector3) -> float:
		var key = [from,to]
		var from_tile = costs.get(key,{}).duplicate()
		var to_tile = costs.get(to,{})
		for type in to_tile:
			from_tile[type] = from_tile.get(type,0.0) + to_tile[type]
		
		var total = 0.0
		for type in weights:
			if typeof(type) == TYPE_VECTOR3:
				total += int(to == type) * weights[type]
			elif from_tile.get(type,0.0):
				total += from_tile.get(type,0.0) * weights[type]
		return total


	func get_neighbors(position:Vector3) -> Array:
		
		var neighbors = []
		
		if neighbor_mask != NEIGHBORS.NONE:
			var N = position + Vector3( 0, 0,-1)
			var S = position + Vector3( 0, 0, 1)
			var E = position + Vector3( 1, 0, 0)
			var W = position + Vector3(-1, 0, 0)
			var U = position + Vector3( 0, 1, 0)
			var D = position + Vector3( 0,-1, 0)
			
			if neighbor_mask & NEIGHBORS.NORTH and bounds.has_point(N):
				neighbors.append(N)
			if neighbor_mask & NEIGHBORS.SOUTH and bounds.has_point(S):
				neighbors.append(S)
			if neighbor_mask & NEIGHBORS.EAST and bounds.has_point(E):
				neighbors.append(E)
			if neighbor_mask & NEIGHBORS.WEST and bounds.has_point(W):
				neighbors.append(W)
			if neighbor_mask & NEIGHBORS.UP and bounds.has_point(U):
				neighbors.append(U)
			if neighbor_mask & NEIGHBORS.DOWN and bounds.has_point(D):
				neighbors.append(D)
		
		for to in edges.get(position,[]):
			if not neighbors.has(to):
				neighbors.append(to)
		
		return neighbors
	
	func add_weights(new_weights:Dictionary):
		for type in new_weights:
			if weights.has(type):
				weights[type] *= new_weights[type]
			else:
				weights[type] = new_weights[type]

	func duplicate(deep:bool = false):
		var n = get_script().new(AABB(bounds.position,bounds.size) )
		n.costs = costs.duplicate(deep)
		n.edges = edges.duplicate(deep)
		n.weights = weights.duplicate(deep)
		return n
	
	func copy(other:Context):
		bounds = AABB(other.bounds.position, other.bounds.size)
		costs = other.costs.duplicate(true)
		edges = other.edges.duplicate(true)
		weights = other.weights.duplicate(true)

class DResult extends Reference:
	export var came_from:Dictionary
	export var costs:Dictionary
	
	func _init(came_from:Dictionary, costs:Dictionary):
		self.came_from = came_from
		self.costs = costs
		
class DPath extends Reference:
	export var costs:Array
	export var total_cost:float
	export var path:Array
	
	func _init(costs:Array = [], total_cost:float = INF, path:Array = []):
		self.costs = costs
		self.total_cost = total_cost
		self.path = path
	
	func _to_string():
		return str({
			"costs": costs,
			"total_cost": total_cost,
			"path": path
		})
		
static func build_dijkstra(context:Context, goals:PoolVector3Array, max_cost:float = INF, flip:bool = false) -> DResult:
	var frontier = PriorityQueue.new()
	var came_from = {}
	var cost_so_far = {}
	
	for position in goals:
		came_from[position] = null
		cost_so_far[position] = 0.0
		frontier.insert(0.0,position)
	
	while not frontier.empty():
		var current:Vector3 = frontier.pop_front()
		
		for next in context.get_neighbors(current):
			var new_cost = cost_so_far[current]
			if flip:
				new_cost += context.get_edge_cost(current,next)
			else:
				new_cost += context.get_edge_cost(next,current)
			
			if not context.has_edge(current, next):
				# not a custom edge, so we add the cost of a normal neighbor
				new_cost += current.distance_to(next)
			
			if not cost_so_far.has(next) or new_cost < cost_so_far.get(next):
				cost_so_far[next] = new_cost
				if new_cost < max_cost:
					
					var smallest_distance = INF
					for position in goals:
						var distance = position.distance_squared_to(next)
						if distance < smallest_distance:
							smallest_distance = distance
					
					frontier.insert(new_cost + smallest_distance,next)
					came_from[next] = current
	
	return DResult.new(came_from, cost_so_far)

static func build_path_from_dijkstra(dijkstra:DResult, position:Vector3, flip:bool = false) -> DPath:
	var path = []
	var costs = []
	var total_cost = 0
	
	var prev = position
	var current = dijkstra.came_from.get(prev)
	
	while current != null:
		var cost = dijkstra.costs[prev] - dijkstra.costs[current]
		
		if flip:
			costs.push_front(cost)
			path.push_front(prev)
		else:
			costs.append(cost)
			path.append(current)
		
		total_cost += cost
		prev = current
		current = dijkstra.came_from.get(prev)
			
	return DPath.new(costs, total_cost, path)


static func astar(context:Context, from:Vector3, to:PoolVector3Array, max_cost:float = INF, flip:bool = false) -> DPath:
	var frontier : = PriorityQueue.new()
	var came_from = {}
	var cost_so_far = {}
	
	for position in to:
		came_from[position] = null
		cost_so_far[position] = 0
		frontier.insert(0,position)
	
	while not frontier.empty():
		var current:Vector3 = frontier.pop_front()
		
		if current == from:
			break
			
		for next in context.get_neighbors(current):
			
			var new_cost = cost_so_far[current]
			if flip:
				new_cost += context.get_edge_cost(current,next)
			else:
				new_cost += context.get_edge_cost(next,current)
			
			if not context.has_edge(current, next):
				# not a custom edge, so we add the cost of a normal neighbor
				new_cost += current.distance_to(next)
			
			if new_cost < cost_so_far.get(next, INF):
				
				cost_so_far[next] = new_cost
				
				if new_cost < max_cost:
					var smallest_distance = INF
					for position in to:
						var distance = position.distance_squared_to(next)
						if distance < smallest_distance:
							smallest_distance = distance
					
					frontier.insert(new_cost + smallest_distance, next)
					came_from[next] = current
	
	return build_path_from_dijkstra(DResult.new(came_from, cost_so_far), from)
