extends Node
class_name State

onready var state_machine = get_parent()
var target

func _enter_state(previous_state:String):
	pass

func _exit_state(new_state:String):
	pass

func _get_transition(delta:float)->String:
	return ''

func _notification(what):
	
	if what == NOTIFICATION_PHYSICS_PROCESS and is_physics_processing():
		var delta = get_physics_process_delta_time()
		var transition = _get_transition(delta)
		if transition:
			var parent = get_parent()
			if parent:
				parent.set_state(transition)
	elif what == NOTIFICATION_PROCESS and is_processing():
		var delta = get_process_delta_time()
		var transition = _get_transition(delta)
		if transition:
			var parent = get_parent()
			if parent:
				parent.set_state(transition)

func _ready():
	set_physics_process(false)
	set_process(false)
	if state_machine:
		target = state_machine.get_parent()
