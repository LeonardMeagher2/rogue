extends Node
class_name StateMachine

var state:String = '' setget set_state
var previous_state:String = ''
	
func set_state(new_state:String):
	previous_state = state
	state = new_state
	
	if previous_state and has_node(previous_state):
		var ps := get_node_or_null(previous_state)
		if ps:
			ps._exit_state(state)
	
	if state and has_node(state):
		var ns := get_node_or_null(state)
		if ns:
			ns._enter_state(previous_state)
