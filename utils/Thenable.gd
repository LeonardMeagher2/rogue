extends Reference
class_name Thenable

signal completed(value)

class Eventual extends Reference:
	var thenable:Thenable
	var target_ref:WeakRef
	var method:String
	
	func _init(thenable:Thenable, target_ref:WeakRef, method:String):
		self.thenable = thenable
		self.target_ref = target_ref
		self.method = method
	
	func execute(value):
		var target = target_ref.get_ref()
		var res = value
		# if no target, we just resolve down the branch using the current value
		if target:
			res = target.call(method, res)
			# if the target call yields we will wait till the function is complete
			while typeof(res) == TYPE_OBJECT and res is GDScriptFunctionState:
				res = yield(res, "completed")
		
		# likely returns a GDScriptFunctionState or a Thenable
		res = thenable.resolve(res)
		while typeof(res) == TYPE_OBJECT and res is GDScriptFunctionState:
			res = yield(res, "completed")
		
		# wait for the thenable to complete so we get the value
		return yield(res, "completed")

var value
var eventuals:Array = []

func resolve(value):
	self.value = value
	
	# These execute in order they were added
	# A brnach of thenables will be executed, then the result
	# from that branch will be passed to the sibling branch
	# in the eventuals array
	
	for eventual in eventuals:
		# a single eventual could be a tree of thenables
		var res = eventual.execute(value)
		while typeof(res) == TYPE_OBJECT and res is GDScriptFunctionState:
			res = yield(res, "completed")
		# set our value to the result of this single branch
		# this value will be used in the execution of the next branch
		self.value = res
		
	
	call_deferred("emit_signal", "completed", self.value)
	return self
	
func then(target:Object = null, method:String = "") -> Thenable:
	var thenable = get_script().new()
	# add new branch
	eventuals.append(Eventual.new(thenable, weakref(target), method))
	# return a new thenable to allow extending the branch
	return thenable
	
static func resolved(value) -> Thenable:
	var thenable = load("res://utils/Thenable.gd").new()
	thenable.call_deferred("resolve", value)
	return thenable
