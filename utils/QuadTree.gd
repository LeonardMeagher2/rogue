extends Reference
class_name QuadTree

export var split_threshold : = 5
export var max_splits : = 2

var current_split : = 0
var quadrants : Array
var _quadrant_instance_pairs = {}

class Quadrant extends Reference:
	const TOP_LEFT = 1
	const TOP_RIGHT = 2
	const BOTTOM_LEFT = 3
	const BOTTOM_RIGHT = 4

	var items : = []
	var bounds : = Rect2()

	func _init(bounds:Rect2 = Rect2()):
		self.bounds = bounds


func get_parent_index(index:int) -> int:
	return index / 4
func get_child_index(index:int, position:int) -> int:
	return index*4+position
func get_level(index:int) -> int:
	return index/(index/4)-1
func split(index:int):
	var start = index*4
	var level = 0
	if index:
		level = index/(index/4)-1
	if level < max_splits:
		if quadrants.size() < start+4:
			quadrants.resize(start+4+1)
		var q = quadrants[start]
		var pos = q.bounds.position
		var half_size = q.bounds.size/2
		quadrants[start+1] = Quadrant.new(Rect2(pos,half_size))
		quadrants[start+2] = Quadrant.new(Rect2(pos+Vector2(half_size.x,0.0),half_size))
		quadrants[start+3] = Quadrant.new(Rect2(pos+Vector2(0.0,half_size.y),half_size))
		quadrants[start+4] = Quadrant.new(Rect2(pos+Vector2(half_size.x,half_size.y),half_size))
func has_children(index:int):
	if index*4+4 > quadrants.size():
		return false
	return quadrants[index*4+4] != null

func get_quadrant(index:int, position:Vector2) -> int:
	if index*4 + 4 < quadrants.size():
		var quad : Quadrant = quadrants[index]
		if not quad:
			return -1
		if position.x > quad.bounds.position.x + quad.bounds.size.x/2:
			if position.y > quad.bounds.position.y  + quad.bounds.size.y/2:
				return index*4 + 3
			else:
				return index*4 + 2
		else:
			if position.y > quad.bounds.position.y + quad.bounds.size.y/2:
				return index*4 + 4
			return index*4 + 1
	return -1

func get_leaf_quadrant(index:int, position:Vector2) -> int:
	if has_children(index):
		var qi = get_quadrant(index,position)
		while has_children(qi):
			qi = get_quadrant(qi,position)
		return qi
	return index

func _recursive_get_all_items(index:int) -> Array:
	var q:Quadrant = quadrants[index]
	var res = []
	
	for item in q.items:
		var item_data = item[1]
		if item_data:
			res.append(item_data)
	if has_children(index):
		for i in 4:
			res += _recursive_get_all_items(get_child_index(index,i+1))
	return res

func find_items(rect:Rect2, index:int = 0) -> Array:
	var q:Quadrant = quadrants[index]
	var res = []
	if q.bounds.intersects(rect):
		if rect.encloses(q.bounds):
			
			# Return everything in this quadrant
			res += _recursive_get_all_items(index)
		else:
			
			for item in q.items:
				var item_position:Vector2 = item[0]
				var item_data = item[1]
				if item_data and rect.has_point(item_position) :
					res.append(item_data)
			if has_children(index):
				for i in 4:
					res += find_items(rect,get_child_index(index,i+1))
	return res

func _init(rect:Rect2):
	quadrants = [Quadrant.new(rect)]

func _ready():
	pass

func add_item(item, position:Vector2):
	if item and not _quadrant_instance_pairs.has(item):
		var index = get_leaf_quadrant(0, position)
		var quadrant = quadrants[index]
		if quadrant.items.size() >= split_threshold:
			split(index)
			index = get_quadrant(index,position)
			quadrant = quadrants[index]
		quadrant.items.append([position,item])
		_quadrant_instance_pairs[item] = [index, quadrant.items.size()-1]

func remove_item(item):
	if item and _quadrant_instance_pairs.has(item):
		var pair = _quadrant_instance_pairs.get(item)
		var index = pair[0]
		if quadrants.size() < index and quadrants[index]:
			var q = quadrants[index]
			# Remove it
			q.items.remove(pair[1])
			_quadrant_instance_pairs.erase(item)
			
func update_item(item, position:Vector2):
	
	if item and _quadrant_instance_pairs.has(item):
		var pair = _quadrant_instance_pairs.get(item)
		
		var new_index = get_leaf_quadrant(0, position)
		var index = pair[0]
		
		if index != new_index:
			if quadrants.size() < index and quadrants[index]:
				var q = quadrants[index]
				q.items.remove(pair[1])
				q = quadrants[new_index]
				
				if q.items.size() >= split_threshold:
					split(new_index)
					new_index = get_quadrant(new_index,position)
					q = quadrants[new_index]
				
				q.items.append([position,item])
				_quadrant_instance_pairs[item] = [index, q.items.size()-1]
		else:
			var q = quadrants[index]
			q.items[pair[1]][0] = position
