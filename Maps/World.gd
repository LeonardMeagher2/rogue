extends Spatial

var selected_player = "Player"
func _ready():
	Saver.connect("load_world", self, "_load", [], CONNECT_REFERENCE_COUNTED)
	Saver.new("testing")
	if not Saver.exists():
		# new world
		Saver.save()
	else:
		# old world
		Saver.load()
		
func _load(world_file:ConfigFile):
	for child in get_children():
		remove_child(child)
		child.queue_free()
		
	var player_info = world_file.get_value("players", selected_player)
	if player_info:
		var map_name = player_info.get("map")
		var map_filepath = "map_data/{name}.cfg".format({"name":map_name})
		var map_data = world_file.get_value("maps", map_name)
		if Saver.exists(map_filepath):
			var map = load(map_data.get("filename")).instance()
			add_child(map)
			map.name = map_name
			map.load_map()
			
			# add player cursor
			var cursor = load("res://Actors/PlayerCursor.tscn").instance()
			map.add_child(cursor)
			var player_node = map.get_node_or_null(selected_player)
			cursor.actor_list.append(cursor.get_path_to(player_node))
			var camera = load("res://Maps/Components/ActorCamera.tscn").instance()
			player_node.add_child(camera)
			
	pass

