extends GridMap
class_name Map

signal save_map(map_file)
signal load_map(map_file)

const Dijkstra = preload("res://utils/Dijkstra.gd")

var pathing_context:Dijkstra.Context = Dijkstra.Context.new(AABB(
	Vector3(-50,-50,-50),
	Vector3(100,100,100)
))

var map_file := ConfigFile.new()

func _enter_tree():
	SharedContext.use({
		"map": self
	})

func get_faction_hostile_nodes(faction:String) -> Array:
	var res = []
	for child in get_children():
		if child is Spatial and Factions.is_faction_hostile_to_node(faction, child):
			res.append(child)
	return res

func get_faction_ally_nodes(faction:String) -> Array:
	var res = []
	for child in get_children():
		if child is Spatial and Factions.is_faction_ally_to_node(faction, child):
			res.append(child)
	return res
	
func get_faction_relationships_to_node(node:Spatial):
	var res = {}
	var Actor = load("res://Actors/Actor.gd")
	for child in get_children():
		if child is Actor:
			var map_position = world_to_map(child.translation)
			var relationship = Factions.get_node_faction_relationship_with_node(node,child)
			if relationship < res.get(map_position,INF):
				res[map_position] = relationship
	return res

func get_used_cells_by_id(id:int) -> PoolVector3Array:
	var result = PoolVector3Array()
	for pos in get_used_cells():
		if get_cell_item(pos.x, pos.y, pos.z) == id:
			result.append(pos)
	return result

func set_walls():
	# This needs to be replaced with map generators, but is useful for testing for now
	if mesh_library:
		var wall_id = mesh_library.find_item_by_name("wall")
		var walls = get_used_cells_by_id(wall_id)
		for position in walls:
			pathing_context.add_cost(position,"wall",1)
	

func _ready():
	call_deferred("set_walls")
	Saver.connect("save_world", self, "_save_world", [], CONNECT_REFERENCE_COUNTED)

func _save_world(world_file:ConfigFile):
	world_file.set_value("maps",name,{
		"filename":filename
	})
	save_map()

func load_map():
	map_file.load(Saver.get_save_path("map_data/{name}.cfg".format({
		"name":name
	})))
	clear()
	var cell_data = map_file.get_value("map","cells", {})
	for cell_id in cell_data:
		for cell in cell_data[cell_id]:
			set_cell_item(cell.x,cell.y,cell.z,cell_id)
	
	set_walls()
	
	var actor_data = map_file.get_value("map", "actors", [])
	for ad in actor_data:
		var actor = load(ad.get("filename")).instance()
		var map_position = ad.get("map_position")
		actor.translation = map_to_world(map_position.x, map_position.y, map_position.z)
		add_child(actor)
		actor.name = ad.get("name")
		actor.load_actor()
	
	emit_signal("load_map")
	
func save_map():
	emit_signal("save_map")
	
	var used_cells = get_used_cells()
	var cell_data = {}
	for cell in used_cells:
		var cell_id = get_cell_item(cell.x, cell.y, cell.z)
		if not cell_data.get(cell_id):
			cell_data[cell_id] = []
		cell_data[cell_id].append(cell)
	map_file.set_value("map","cells",cell_data)
	
	var actors = []
	var Actor = load("res://Actors/Actor.gd")
	for child in get_children():
		if child is Actor:
			actors.append({
				"name":child.name,
				"filename": child.filename,
				"script": child.get_script().resource_path,
				"map_position": world_to_map(child.translation)
			})
	map_file.set_value("map","actors",actors)
	
	var dir = Directory.new()
	if not dir.dir_exists(Saver.get_save_path("map_data")):
		dir.make_dir_recursive(Saver.get_save_path("map_data"))
	map_file.save(Saver.get_save_path("map_data/{name}.cfg".format({
		"name": name	
	})))
	
