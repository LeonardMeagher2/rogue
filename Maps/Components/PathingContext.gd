extends Resource
class_name PathingContext

export var costs = {}
export var edges = {}
export var weights = {}

func _default_tile() -> Dictionary:
	return {}

func add_cost(position:Vector2, type:String,amount:float) -> void:
	if not costs.has(type):
		costs[type] = []
	if not costs.has(position):
		costs[position] = _default_tile()
	var w = costs.get(position)
	w[type] = w.get(type,0.0) + amount
	
	if costs[type].has(position):
		if w[type] == 0.0:
			costs[type].erase(position)
	else:
		costs[type].append(position)

func get_costs(position:Vector2, types:PoolStringArray = []):
	var w = costs.get(position,_default_tile())
	if types.size() == 0:
		return w
	var total = 0.0
	for type in types:
		total += w.get(type,0.0)
	return total

func get_costs_weighted(position:Vector2, weights:Dictionary = {}) -> float:
	var w = costs.get(position,_default_tile())
	var total = 0.0
	if weights.size() == 0:
		for type in w:
			total += w[type]
	else:
		for type in weights:
			if typeof(type) == TYPE_VECTOR2:
				total += int(position == type) * weights[type]
			elif w.get(type,0.0):
				total += w.get(type,0.0) * weights[type]
	return total
	
func add_edge_cost(from:Vector2, to:Vector2, type:String, amount:float, bi_directional = false) -> void:
	var key = [from,to]
	
	add_edge(from,to)
	
	if not costs.has(type):
		costs[type] = []
	if not costs.has(key):
		costs[key] = _default_tile()
	var w = costs.get(key)
	w[type] = w.get(type,0.0) + amount
	
	if costs[type].has(to):
		if w[type] == 0.0:
			costs[type].erase(to)
	else:
		costs[type].append(to)
		
	if bi_directional:
		add_edge_cost(to,from,type,amount,false)

func get_edge_costs(from:Vector2, to:Vector2, types:PoolStringArray = []):
	var key = [from,to]
	var w = costs.get(key,_default_tile()).duplicate()
	var to_w = costs.get(to,_default_tile())
	for type in to_w:
		w[type] = w.get(type,0.0) + to_w[type] 
	
	if types.size() == 0:
		return w
	var total = 0.0
	for type in types:
		total += w.get(type,1.0)
	return total

func get_edge_costs_weighted(from:Vector2, to:Vector2) -> float:
	var key = [from,to]
	var w = costs.get(key,_default_tile()).duplicate()
	var to_w = costs.get(to,_default_tile())

	for type in to_w:
		w[type] = w.get(type,0.0) + to_w[type]
	
	var total = 0.0
	if weights.size() == 0:
		for type in w:
			total += w[type]
	else:
		for type in weights:
			if typeof(type) == TYPE_VECTOR2:
				total += int(to == type) * weights[type]
			elif w.get(type,0.0):
				total += w.get(type,0.0) * weights[type]
	return total

func add_edge(from:Vector2, to:Vector2, bi_directional = false) -> void:
	if not edges.has(from):
		edges[from] = []
	edges[from].append(to)
	if bi_directional:
		add_edge(to,from,false)

func remove_edge(from:Vector2, to:Vector2, bi_directional = false) -> void:
	var key = [from,to]
	if edges.has(from):
		edges[from].erase(to)
		if edges[from].size() == 0:
			edges.erase(from)
	
	if costs.has(key):
		costs.erase(key)
		
	if bi_directional:
		remove_edge(to,from,false)

func add_weights(new_weights):
	for type in new_weights:
		if weights.has(type):
			weights[type] *= new_weights[type]
		else:
			weights[type] = new_weights[type]

func get_positions_by_type(types:PoolStringArray) -> Array:
	var positions : = []
	var defaults = _default_tile()
	for type in types:
		if costs.has(type):
			for position in costs.get(type):
				var w = costs.get(position,defaults).get(type,0.0)
				if not positions.has(position) and w:
					positions.append(position)
	return positions
	
	
func duplicate(deep:bool = false):
	var n = get_script().new()
	n.costs = costs.duplicate(deep)
	n.edges = edges.duplicate(deep)
	n.weights = weights.duplicate(deep)
	return n
