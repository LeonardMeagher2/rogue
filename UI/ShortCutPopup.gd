extends Popup
class_name ShortCutPopup

export(ShortCut) var shortcut = ShortCut.new()

func _unhandled_input(event):
	
	if event.is_pressed() and not event.is_echo() and shortcut.is_valid() and shortcut.is_shortcut(event):
		if is_visible_in_tree():
			hide()
			get_tree().set_input_as_handled()
		else:
			if not (get_viewport().get_modal_stack_top() and not get_viewport().get_modal_stack_top().is_a_parent_of(self)):
				popup_centered_ratio()
		
	if is_visible_in_tree():
		get_tree().set_input_as_handled()
