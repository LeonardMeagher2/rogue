extends Popup

signal item_selected(data)

onready var item_list = $PanelContainer/MarginContainer/VBoxContainer/ItemList
onready var label = $PanelContainer/MarginContainer/VBoxContainer/HBoxContainer/Label
onready var label_texture = $PanelContainer/MarginContainer/VBoxContainer/HBoxContainer/TextureRect

var was_visible = false

func _ready():
	connect("visibility_changed", self, "_visibility_changed")
	item_list.connect("item_selected", self, "_item_selected")

func clear():
	item_list.clear()
	set_title()

func set_title(node_path:NodePath = @"", action:String = "Interact"):
	var title = action
	if node_path:
		var node = get_node_or_null(node_path)
		title += " with {name}".format({
			"name": node.name
		})
		var tex := ImageTexture.new()
		Utils.capture_node_image(tex, Vector2(16,16), node)
		label_texture.texture = tex
		label_texture.rect_min_size = Vector2(16,16)
		get_tree().connect("idle_frame", label_texture, "update", [], CONNECT_ONESHOT)
	else:
		label_texture.texture = null
		label_texture.rect_min_size = Vector2()
	label.text = title

func add_node_item(node_path:NodePath):
	var node = get_node_or_null(node_path)
	var tex := ImageTexture.new()
	Utils.capture_node_image(tex, Vector2(16,16), node)
	var index = item_list.get_item_count()
	item_list.add_item(node.name, tex, true)
	item_list.set_item_metadata(index, node_path)
	
	get_tree().connect("idle_frame", item_list, "update", [], CONNECT_ONESHOT)
	
func add_job_item(job:Job):
	var index = item_list.get_item_count()
	item_list.add_item(job.name, null, true)
	item_list.set_item_metadata(index, job)
	
func get_item_count():
	return item_list.get_item_count()

func _unhandled_input(event):
	if was_visible and not visible:
		accept_event()
	was_visible = is_visible_in_tree()
	if was_visible:
		accept_event()

func _item_selected(index):
	var data = item_list.get_item_metadata(index)
	emit_signal("item_selected", data)
	hide()
	
func _visibility_changed():
	if not is_visible_in_tree():
		emit_signal("item_selected", null)
	
