extends Control

func _on_MainMenu_pressed():
	TurnManager.set_process(false)
	Saver.save()
	get_tree().change_scene("res://Main.tscn")

func _on_Save_pressed():
	Saver.save()
