extends Spatial
class_name Actor

signal save(actor_file)
signal load(actor_file)

var ctx:SharedContext.Context

func _enter_tree():
	ctx = SharedContext.use({
		"actor": self,
		"is_dead": false
	})

func _ready():
	Saver.connect("save_world", self, "_save", [], CONNECT_REFERENCE_COUNTED)
func _save(world_save:ConfigFile):
	save_actor()

func save_actor():
	var actor_file = ConfigFile.new()
	
	actor_file.set_value("actor", "groups", get_groups())
	
	var components = []
	var Component = load("res://Components/Component.gd")
	for child in get_children():
		if child is Component:
			components.append({
				"name": child.name,
				"script": child.get_script().resource_path
			})
	actor_file.set_value("actor", "components", components)
	
	emit_signal("save", actor_file)
	
	var dir = Directory.new()
	if not dir.dir_exists(Saver.get_save_path("actor_data")):
		dir.make_dir_recursive(Saver.get_save_path("actor_data"))
	actor_file.save(Saver.get_save_path("actor_data/{name}.cfg".format({
		"name": name	
	})))

func load_actor():
	var actor_file = ConfigFile.new()
	actor_file.load(Saver.get_save_path("actor_data/{name}.cfg".format({
		"name": name
	})))
	
	for group in actor_file.get_value("actor","groups",[]):
		add_to_group(group)
	
	var components = actor_file.get_value("actor", "components")
	if components:
		var Component = load("res://Components/Component.gd")
		for child in get_children():
			if child is Component:
				remove_child(child)
				child.queue_free()
		for component in components:
			var node = load(component.get("script")).new()
			add_child(node)
			node.name = component.get("name")
	
	ctx.call_deferred("fire","load", actor_file)
