extends Spatial

export(Array,NodePath) var actor_list
export var device:int = 0
export var max_speed = 20

var ctx:SharedContext.Context
var movement = Vector2()
var speed = 0
var accel = 20
var decel = 200

onready var map_position:Vector3 setget set_map_position,get_map_position


func set_map_position(v:Vector3):
	map_position = v
	if ctx:
		translation = ctx.map.map_to_world(map_position.x, map_position.y, map_position.z)
	
func get_map_position():
	if ctx:
		return ctx.map.world_to_map(translation)
	return null

func move(movement:Vector3):
	if ctx:
		set_map_position(map_position + movement)

func _ready():
	TurnManager.connect("turn_started",self,"turn_started", [], CONNECT_REFERENCE_COUNTED)
	TurnManager.connect("turn_ended",self,"turn_ended", [], CONNECT_REFERENCE_COUNTED)
	
func turn_started(node_path:NodePath):
	
	var behavior = get_node_or_null(node_path) as Behavior
	ctx = SharedContext.from(behavior)
	
	if behavior and get_path_to(ctx.actor) in actor_list:
		pause(false)
	else:
		pause(true)
		ctx = null

func turn_ended(node_path:NodePath):
	if node_path in actor_list:
		pause(true)
		ctx = null
	
func pause(paused:bool = true):
	set_process_unhandled_input(not paused)
	set_process(not paused)

func is_paused():
	return not (is_processing_unhandled_input() and is_processing())

func _process(delta):
	if movement:
		speed += accel * delta
	else:
		speed -= decel * delta
	speed = clamp(speed,0.0,max_speed)
	var frame_movement = movement.round() * speed * delta
	move(Vector3(frame_movement.x, 0.0, frame_movement.y))

func _unhandled_input(event):
	if not event.device == device:
		return
	
	var behavior = TurnManager.get_current_node() as Behavior

	if not behavior:
		pause(true)
		ctx = null
		return
	
	if event.is_action_pressed("wait"):
		behavior.current_turn_ap = 0.0
		
	handle_movement(event)
	handle_interactions(event)
		
func handle_movement(event):
	if not event.device == device:
		return
		
	var prev_movement = movement
	
	if event.is_action("move_east") or event.is_action("move_west"):
		movement.x = event.get_action_strength("move_east") - event.get_action_strength("move_west")
	if event.is_action("move_north") or event.is_action("move_south"):
		movement.y = event.get_action_strength("move_south") - event.get_action_strength("move_north")
	
	if prev_movement == Vector2() and movement != Vector2():
		# Quick jump, if the cursor was standing still
		map_position = map_position.floor() + (Vector3(movement.x, 0.0, movement.y) * 0.5).ceil()
		
	
func handle_interactions(event):
	if event.is_action_pressed("interact"):
		$InteractionLongPressTimer.start()
	
	elif event.is_action_released("interact"):
		
		if not $InteractionLongPressTimer.is_stopped():
			# we're in the middle of a long press, lets stop the timer
			$InteractionLongPressTimer.stop()
			pause(true)
			var nodes_at_cursor = $InteractionArea.get_overlapping_bodies()
			var jobs = []
			
			if nodes_at_cursor.size() == 0:
				ctx.fire("get_possible_jobs", [{
					"actor_path": @"",
					"map_position": get_map_position(),
					"behavior": ctx.behavior,
					"jobs": jobs
				}])
			
			elif nodes_at_cursor.size() == 1:
				var selected_actor = nodes_at_cursor[0]
				
				if selected_actor:
					var selected_actor_context = SharedContext.from(selected_actor)
					
					ctx.fire("get_possible_jobs", [{
						"actor_path": selected_actor.get_path(),
						"map_position": get_map_position(),
						"behavior": ctx.behavior,
						"jobs": jobs
					}])
					selected_actor_context.fire("get_possible_jobs", [{
						"actor_path": selected_actor.get_path(),
						"map_position": get_map_position(),
						"behavior": ctx.behavior,
						"jobs": jobs
					}, ctx.actor.get_path()])
			else:
				_on_InteractionLongPressTimer_timeout()
				return
			
			var job_stack = PriorityQueue.new()
			
			while jobs.size() > 0:
				var job = jobs.pop_front()
				ctx.behavior.add_child(job)
				var cost = job.compute_minimum_cost()
				
				if cost <= ctx.behavior.current_turn_ap:
					job_stack.insert(cost,job)
				else:
					ctx.behavior.remove_child(job)
					if job.free_on_exit:
						job.queue_free()
			
			if not job_stack.empty():
				job_stack.pop_front()
				while not job_stack.empty():
					var job = job_stack.pop_front()
					ctx.behavior.remove_child(job)
					if job.free_on_exit:
						job.queue_free()
			pause(false)
		
func _on_InteractionLongPressTimer_timeout():
	# long press on interaction was done
	if is_paused():
		return
	pause(true)
	var behavior = TurnManager.get_current_node() as Behavior
	if not behavior:
		return
	
	var node_select = $"/root/Play/UI/NodeSelect"
	
	var jobs = []
	ctx.fire("get_possible_jobs", [{
		"actor_path": @"",
		"map_position": get_map_position(),
		"behavior": ctx.behavior,
		"jobs": jobs
	}])
	var nodes_at_cursor = $InteractionArea.get_overlapping_bodies()
	
	node_select.clear()
	for job in jobs:
		node_select.add_job_item(job)
	for node in nodes_at_cursor:
		if not node == ctx.actor:
			node_select.add_node_item(node.get_path())
	
	if node_select.get_item_count():
		node_select.connect("item_selected", self, "_on_item_select", [behavior, node_select], CONNECT_ONESHOT)
		node_select.popup_centered()
	else:
		pause(false)

func _on_item_select(selection, behavior, node_select):
	pause(false)

	if selection is NodePath:
		pause(true)
		var selected_actor = get_node_or_null(selection)
		if selected_actor:
			var selected_actor_context = SharedContext.from(selected_actor)
			var jobs = []
			ctx.fire("get_possible_jobs", [{
				"actor_path": selection,
				"map_position": get_map_position(),
				"behavior": behavior,
				"jobs": jobs
			}])
			selected_actor_context.fire("get_possible_jobs", [{
				"actor_path": selection,
				"map_position": get_map_position(),
				"behavior": behavior,
				"jobs": jobs
			}, ctx.actor.get_path()])
			
			if jobs.size():
				node_select.clear()
				for job in jobs:
					node_select.add_job_item(job)
				yield(get_tree(),"idle_frame")
				node_select.set_title(selection)
				node_select.popup_centered()
				node_select.connect("item_selected", self, "_on_item_select", [behavior, node_select], CONNECT_ONESHOT)
			else:
				pause(false)
	elif selection is Job:
		behavior.add_child(selection)

