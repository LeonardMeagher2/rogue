extends MeshInstance

var ctx:SharedContext.Context

func _enter_tree():
	ctx = SharedContext.use()

func _ready():
	ctx.on("dead", self, "dead")

func dead():
	var mat = get_surface_material(0) as ShaderMaterial
	if mat:
		mat.set_shader_param("modulate", Color.red)
